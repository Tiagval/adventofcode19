pub struct Intcode{
    code: Vec<i32>,
    iterator : usize,
    pub input : Vec<i32>,
    pub output: i32
}

impl Intcode{
    pub fn new(code : &[i32]) -> Intcode{
        Intcode{
            code : code.to_vec(),
            iterator: 0,
            input: Vec::new(),
            output : -1,
        }
    }

    fn operate(&mut self, opcode: u32, arr: &[i32;3]) -> i32 {
        match opcode{
            1 => {
                let dest = arr[2] as usize;
                self.code[dest] = arr[0] + arr[1];
                4
            },
            2 => {
                let dest = arr[2] as usize;
                self.code[dest] = arr[0]*arr[1];
                4
            },
            3 => {
                if self.input.len() == 0{
                    return 0;
                }
                else{
                    let dest = arr[0] as usize;
                    let val = self.input.remove(0);
                    self.code[dest] = val;
                }
                2
            },
            4 => {
                self.output = self.code[arr[0] as usize];
                2
            },
            5 => {
                if arr[0] != 0 {
                    arr[1] - self.iterator as i32
                }
                else {
                    3
                }
            }
            6 => {
                if arr[0] == 0{
                    arr[1] - self.iterator as i32
                }
                else{
                    3
                }
            }
            7 => {
                let dest = arr[2] as usize;
                if arr[0] < arr[1]{
                    self.code[dest] = 1;
                }
                else{
                    self.code[dest] = 0;
                }
                4
            }
            8 => {
                let dest = arr[2] as usize;
                if arr[0] == arr[1]{
                    self.code[dest] = 1;
                }
                else{
                    self.code[dest] = 0;
                }
                4
            }
            _ => {println!("Dafuq is going on!?"); return -999},
        }
    }

    fn read_param_mode(mut curr : String) -> (String, String){
        let str_size = curr.len() as usize;
        if str_size == 1{
            return (curr.to_string(), "00".to_string());
        }
        let opcode = &mut curr[str_size-2..].to_string();
        let modes = &mut curr[..str_size-2].to_string();
        if modes.len() < 3{
            modes.insert_str(0,&"0".repeat(2 - modes.len()))
        }
        (opcode.to_string(), modes.to_string())
    }

    fn get_values_array(&self,modes: &String, opcode: &String) -> [i32;3]{
        let mut value_arr : [i32 ; 3] = [0 ; 3];
        for (i, mode) in modes.chars().rev().enumerate(){
            let dest = self.iterator + i;
            if (3..5).contains(&opcode.parse::<u32>().unwrap()){
                value_arr[0] = self.code[dest + 1];
                break;
            }
            if mode == '0'{
                let pointer = self.code[dest + 1] as usize;
                value_arr[i] = self.code[pointer];
            }
            else if mode == '1'{
                value_arr[i] = self.code[dest + 1];
            }
            else{
                println!("Invalid mode of operation: {}", mode);
            }
        }
        if !(3..5).contains(&opcode.parse::<u32>().unwrap()){
            let dest = self.iterator + 3;
            value_arr[2] = self.code[dest];
        }
        value_arr
    }

    pub fn iterate(&mut self) -> u32{
        loop{
            let current = self.code[self.iterator];
            if current == 99 {
                return 1;
            }
            let (opcode, modes) = Intcode::read_param_mode(current.to_string());
            let value_arr = self.get_values_array(&modes, &opcode);
            let inc = self.operate(opcode.parse::<u32>().unwrap(), &value_arr);
            if inc == 0 {
                return 0;
            }
            else{
                self.iterator = (self.iterator as i32 + inc) as usize;
            }
        }
    }
}
