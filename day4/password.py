n_range = [359282,820401]

n_passwd = 0
start = n_range[0]
end = n_range[1] + 1

class Password:

    def __init__(self, number):
        self.number = str(number)

    def has_double(self):
        """Receives string with numbers, checks for two equal adjacent digits"""
        for i in range(len(self.number) - 1):
            if self.number[i] == self.number[i + 1]:
                if i > 0 and self.number[i - 1] == self.number[i]:
                    continue
                elif i < len(self.number) - 2 and self.number[i] == self.number[i + 2]:
                    continue
                else:
                    return True
        else:
            return False

    def has_decrease(self):
        """Receives string with numbers, checks if ajdacent digits decrease"""
        for i in range(len(self.number) - 1):
            if int(self.number[i]) > int(self.number[i + 1]):
                return True
        else:
            return False

for i in range(start, end):
    passwd = Password(i)
    if not passwd.has_decrease() and passwd.has_double():
        n_passwd += 1

print(n_passwd)
