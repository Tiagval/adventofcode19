input = open('input.txt','r')

def fuel(mass):
    return max(0,int(mass/3) -2)

#We need to calculate fuel for the fuel, dog
def fuel_for_fuel(mass):
    total = 0
    while mass > 0:
        total += fuel(mass)
        mass = fuel(mass)
    return total

total = 0
for mass in input:
    total += fuel_for_fuel(int(mass))

print(total)
