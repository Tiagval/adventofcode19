def make_layers(input_data, width, height):
    input_data = input_data.strip()
    layers = {}
    inc = 0
    read_layers = 1 #hack to start at 1
    while inc < len(input_data):
        for _ in range(height):
            if read_layers not in layers:
                layers[read_layers] = [input_data[inc:inc+width]]
            else:
                layers[read_layers].append(input_data[inc:inc+width])
            inc += width
        read_layers += 1
    return layers

test_image = make_layers('123456789012',3,2)
input_data = open('input.txt','r').read()

image = make_layers(input_data,25,6)

def get_digits(image):
    image = {k: v for k, v in image.items() if v is not None}
    min_zeros = 999999999999999
    for pixels in image.values():
        n_zeros = 0
        for str in pixels:
            n_zeros += str.count('0')
        if n_zeros < min_zeros:
            min_zeros = n_zeros
            min_pixels = pixels
    print(min_pixels)
    ones, twos = 0, 0
    for str in min_pixels:
        ones += str.count('1')
        twos += str.count('2')
    return ones*twos

#print(get_digits(image))

def decode(image, width, height):
    decoded = ['2'*width]*height
    for layer in image.values():
        for i, row in enumerate(layer):
            for j, pixel in enumerate(row):
                if pixel != 2 and decoded[i][j] == '2':
                    decoded[i] = decoded[i][:j] + pixel + decoded[i][j+1:]
    return decoded

decoded = decode(image, 25, 6)

def print_message(image):
    ascii_art = {'0' : '#', '1' : '*', '2' : ' '}
    message = ''
    for row in image:
        for pixel in row:
            message += ascii_art[pixel]
        message += '\n'
    return message

print(print_message(decoded))
