input = open('input.txt','r').read()
target = 19690720

class Opcode:

    def __init__(self, in_state, noun, verb):
        self.in_state = in_state.split(',')
        self.noun = noun
        self.verb = verb

    def fix_input(self):
        """Set noun and verb in in_state"""
        self.in_state[1] = self.noun
        self.in_state[2] = self.verb
        self.in_state = list(map(int, self.in_state))

    def operate(self, in_list):
        """Apply operation on input list"""
        if in_list[0] == 1:
            return sum(in_list[1:])
        elif in_list[0] == 2:
            return in_list[1]*in_list[2]
        else:
            return -1

    def get_in_list(self, iter):
        """Get values from adresses"""
        el1 = self.in_state[iter+1]
        el2 = self.in_state[iter+2]
        return[self.in_state[iter], self.in_state[int(el1)], self.in_state[int(el2)]]

    def iterate(self):
        """Go through opcode until it hits a 99"""
        self.fix_input()
        keep_going = True
        i = 0
        while keep_going:
            out_bit = self.operate(self.get_in_list(i))
            if out_bit > 0:
                self.in_state[self.in_state[i+3]] = out_bit
                i += 4
            else:
                keep_going = False

        return self.in_state[0]

#Brute force for now
def get_noun_verb(target):
    """Try all values until it matches a target"""
    #This could be optimzed
    while True:
        for i in range(100):
            for j in range(100):
                opcode = Opcode(input, i, j)
                out = opcode.iterate()
                del opcode
                if int(out) == target:
                    return 100*i + j
        break

print(get_noun_verb(target))
