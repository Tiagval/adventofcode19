input = open('input.txt','r').read()

class Wire:

    def __init__(self, path):
        self.path = path

    def increment_position(self, step, start):
        """Increment position by given step"""
        #Step of the format U120, R723, i.e. direction followed by step
        dir = step[0]
        step = int(step[1:])
        if dir == 'U':
            start[1] += step
        elif dir == 'D':
            start[1] -= step
        elif dir == 'R':
            start[0] += step
        elif dir == 'L':
            start[0] -= step
        return start

    def get_step_direction(self,line):
        if line[0] != line[2]:
            return 'H'
        if line[1] != line[3]:
            return 'V'

    def does_it_cross(self,first_line, second_line):
        """Determine if two lines are crossing"""
        dir = self.get_step_direction(second_line)
        cross = -1
        if dir == 'V':
            if min(first_line[0], first_line[2]) < second_line[0] < max(first_line[0], first_line[2]):
                if (min(second_line[1],second_line[3]) < first_line[1] < max(second_line[1],second_line[3])):
                    cross = [second_line[0],first_line[1]]

        elif dir == 'H':
            if min(second_line[0], second_line[2]) < first_line[0] < max(second_line[0], second_line[2]):
                if min(first_line[1], first_line[3]) < second_line[1] < max(first_line[1], first_line[3]):
                    cross = [first_line[0],second_line[1]]
        return cross

    def correct_for_cross(self, line, cross):
        dir = self.get_step_direction(line)
        return abs(line[2] - cross[0]) if dir == 'H' else abs(line[3] - cross[1])

    def get_cross_points(self,second_wire_path):
        """Get intersections step by step"""
        cross_points = []
        first_line = [0,0,0,0] #x1,y1,x2,y2
        sum_first_steps = 0
        for first_wire_step in self.path:
            sum_first_steps += int(first_wire_step[1:])
            second_line = [0,0,0,0]
            sum_second_steps = 0
            first_line[2:] = self.increment_position(first_wire_step, first_line[:2])
            for second_wire_step in second_wire_path:
                second_line[2:] = self.increment_position(second_wire_step,second_line[:2])
                sum_second_steps += int(second_wire_step[1:])
                if self.get_step_direction(first_line) != self.get_step_direction(second_line):
                    cross = self.does_it_cross(first_line, second_line)
                    if cross != -1:
                        first_inc = self.correct_for_cross(first_line, cross)
                        second_inc = self.correct_for_cross(second_line, cross)
                        sum_first_steps -= first_inc
                        sum_second_steps -= second_inc
                        steps = sum_first_steps + sum_second_steps
                        cross_points.append([cross,steps])
                        sum_first_steps += first_inc
                        sum_second_steps += second_inc
                second_line[:2] = second_line[2:] #current point in the new starting point
            first_line[:2] = first_line[2:]
        return cross_points

first_wire_path = input.split('\n')[0].split(',')
second_wire_path = input.split('\n')[1].split(',')

wire = Wire(first_wire_path)
x_points = wire.get_cross_points(second_wire_path)

def taxicab(point):
    return abs(point[0]) + abs(point[1]) #starting point is 0,0

taxi_points = [taxicab(x[0]) for x in x_points]

steps_list = [x[1] for x in x_points]

print(min(taxi_points), min(steps_list))
