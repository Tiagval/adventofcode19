input = open('input.txt', 'r').read()

class Intcode:

    def __init__(self, in_state, input):
        self.in_state = in_state.split(',')
        self.input = input
        self.iter = 0

    def fix_input(self):
        """Set noun and verb in in_state"""
        self.in_state = list(map(int, self.in_state))

    def read_param_mode(self, iter):
        """Split large opcode"""
        inst = str(self.in_state[iter])
        opcode = inst[-2:]
        modes = inst[:-2]
        return opcode, modes

    def get_with_param_mode(self,iter,modes):
        """Return values by position or immediate mode"""
        values = []
        for i, mode in enumerate(modes[::-1]):
            if mode == '0':
                values.append(self.in_state[self.in_state[iter+i+1]])
            if mode == '1':
                values.append(self.in_state[iter+i+1])
        return values

    def operate(self, opcode, in_list = []):
        """Apply operation on input list"""
        if opcode == 1:
            return in_list[0] + in_list[1]

        elif opcode == 2:
            return in_list[0]*in_list[1]

        elif opcode == 3:
            return self.input

        elif opcode == 4:
            return in_list[0]

        elif opcode == 5:
            if in_list[0] != 0:
                return in_list[1] - self.iter
            else:
                return 3

        elif opcode == 6:
            if in_list[0] == 0:
                return in_list[1] - self.iter
            else:
                return 3

        elif opcode == 7:
            if in_list[0] < in_list[1]:
                return 1
            else:
                return 0

        elif opcode == 8:
            if in_list[0] == in_list[1]:
                return 1
            else:
                return 0

        elif opcode == 99:
            return -1
        else:
            print(self.iter, self.in_state[self.iter], self.in_state)
            exit()

    def iterate(self):
        """Go through in_state until it hits a 99"""
        self.fix_input()
        while True:
            current = self.in_state[self.iter]
            if current == 99:
                print('Finished')
                return
            el1 = self.in_state[self.iter + 1]
            el2 = self.in_state[self.iter + 2]
            el3 = self.in_state[self.iter + 3]
            if current < 3:
                self.in_state[el3] = self.operate(current, [self.in_state[el1], self.in_state[el2]])
                self.iter += 4
            elif current == 3:
                self.in_state[el1] = self.operate(current)
                self.iter += 2
            elif current == 4:
                print(self.operate(current,[self.in_state[el1]]))
                self.iter += 2
            elif 4 < current < 7:
                self.iter += self.operate(current, [el1, el2])
            elif 6 < current < 9:
                self.in_state[el3] = self.operate(current, [self.in_state[el1], self.in_state[el2]])
                self.iter += 4
            else:
                opcode, modes = self.read_param_mode(self.iter)
                if int(opcode) != 4 and len(modes) == 1:
                    modes = '0' + modes
                values = self.get_with_param_mode(self.iter, modes)
                if int(opcode) == 3:
                    self.in_state[values[0]] = self.operate(int(opcode))
                    inc = 2
                elif int(opcode) == 4:
                    print(self.operate(int(opcode),[values[0]]))
                    inc = 2
                elif 4 < int(opcode) < 7:
                    inc = self.operate(int(opcode), values)
                else:
                    self.in_state[el3] = self.operate(int(opcode), values)
                    inc = 4
                self.iter += inc

#input = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"
#input = '3,9,8,9,10,9,4,9,99,-1,8'
intcode = Intcode(input, 5)
intcode.iterate()
