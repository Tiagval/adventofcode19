input = open('input.txt','r').read()

class Orbits:

    def __init__(self,map):
        self.orbits = self.parser(map)

    def parser(self,map):
        map = filter(None,map)
        orbits = {}
        for line in map:
            objects = line.split(')')
            if objects[1] not in orbits:
                orbits[objects[1]] = [objects[0]]
            else:
                orbits[objects[1]].append(objects[0])
        return orbits

    def path_to_dest(self, start, dest):
        got_to_dest = False
        steps = 0
        sons = []
        son = self.orbits[start][0]
        while not got_to_dest:
            if son == dest:
                got_to_dest= True
            elif son == 'COM':
                return 'Got to COM'
            else:
                steps += 1
                son = self.orbits[son][0]
                sons.append(son)
        return steps, sons

    def find_inter(self, obj1, obj2):
        steps1, sons1 = self.path_to_dest(obj1,'COM')
        steps2, sons2 = self.path_to_dest(obj2,'COM')
        for son in sons1:
            if son in sons2:
                return son

    def get_dist(self, start1, start2):
        inter = self.find_inter(start1, start2)
        dist1 = self.path_to_dest(start1, inter)[0]
        dist2 = self.path_to_dest(start2, inter)[0]
        print(dist1, dist2)
        return dist1 + dist2

orbits = Orbits(input.split('\n'))
print(orbits.get_dist('YOU','SAN'))
