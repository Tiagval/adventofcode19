input = open('input.txt', 'r').read()

class Intcode:

    def __init__(self, in_state):
        self.in_state = in_state.split(',')
        self.iter = 0
        self.input = []
        self.output = -1
        self.fix_input()

    def fix_input(self):
        """Set noun and verb in in_state"""
        self.in_state = list(map(int, self.in_state))

    def read_param_mode(self, iter):
        """Split large opcode"""
        inst = str(self.in_state[iter])
        opcode = inst[-2:]
        modes = inst[:-2]
        return opcode, modes

    def get_with_param_mode(self,iter,modes):
        """Return values by position or immediate mode"""
        values = []
        for i, mode in enumerate(modes[::-1]):
            if mode == '0':
                values.append(self.in_state[self.in_state[iter+i+1]])
            if mode == '1':
                values.append(self.in_state[iter+i+1])
        return values

    def operate(self, opcode, in_list = []):
        """Apply operation on input list"""
        if opcode == 1:
            return in_list[0] + in_list[1]

        elif opcode == 2:
            return in_list[0]*in_list[1]

        elif opcode == 3:
            if len(self.input) == 0:
                return -1
            ret = self.input[0]
            self.input.pop(0)
            return ret

        elif opcode == 4:
            return in_list[0]

        elif opcode == 5:
            if in_list[0] != 0:
                return in_list[1] - self.iter
            else:
                return 3

        elif opcode == 6:
            if in_list[0] == 0:
                return in_list[1] - self.iter
            else:
                return 3

        elif opcode == 7:
            if in_list[0] < in_list[1]:
                return 1
            else:
                return 0

        elif opcode == 8:
            if in_list[0] == in_list[1]:
                return 1
            else:
                return 0

        elif opcode == 99:
            return -1
        else:
            print(self.iter, self.in_state[self.iter], self.in_state)
            exit()


    def iterate(self, inputs):
        """Go through in_state until it hits a 99"""
        self.input = inputs
        #print(self.iter)
        #print(self.input)
        while True:
            current = self.in_state[self.iter]
            #print('curr,iter',current, self.iter)
            if current == 99:
                #print('Finished')
                return self.output, 1
            el1 = self.in_state[self.iter + 1]
            el2 = self.in_state[self.iter + 2]
            el3 = self.in_state[self.iter + 3]
            if current < 3:
                self.in_state[el3] = self.operate(current, [self.in_state[el1], self.in_state[el2]])
                self.iter += 4
            elif current == 3:
                operate_out = self.operate(current)
                if operate_out == -1:
                    #print(self.iter, current)
                    return self.output, -1
                else:
                    self.in_state[el1] = operate_out
                self.iter += 2
            elif current == 4:
                out = self.operate(current,[self.in_state[el1]])
                #print(out)
                self.output = out
                self.iter += 2
            elif 4 < current < 7:
                self.iter += self.operate(current, [el1, el2])
            elif 6 < current < 9:
                self.in_state[el3] = self.operate(current, [self.in_state[el1], self.in_state[el2]])
                self.iter += 4
            else:
                opcode, modes = self.read_param_mode(self.iter)
                if int(opcode) != 4 and len(modes) == 1:
                    modes = '0' + modes
                values = self.get_with_param_mode(self.iter, modes)
                if int(opcode) == 3:
                    opcode_out = self.operate(int(opcode))
                    if opcode_out == -1:
                        #print(self.iter, current)
                        return self.output, -1
                    else:
                        self.in_state[values[0]] = self.operate(int(opcode))
                    inc = 2
                elif int(opcode) == 4:
                    out = self.operate(int(opcode),[values[0]])
                    #print(out)
                    self.output = out
                    inc = 2
                elif 4 < int(opcode) < 7:
                    inc = self.operate(int(opcode), values)
                else:
                    self.in_state[el3] = self.operate(int(opcode), values)
                    inc = 4
                self.iter += inc


def get_out(sequence):
    prev = 0
    amps = None
    amps = [Intcode(input) for i in range(len(sequence))]
    not_finished = True
    gave_phase = False
    while not_finished:
        for i, phase in enumerate(sequence):
            #inputs = [phase,prev]
            if not gave_phase:
                inputs = [phase,prev]
            else:
                inputs = [prev]
            out, finish = amps[i].iterate(inputs)
            prev = out
            if phase == sequence[-1]:
                last_out = prev
            if finish == 1:
                return last_out
        gave_phase = True

def give_sequence(sequence):
    if len(sequence) <= 1:
        yield sequence
    else:
        for seq in give_sequence(sequence[1:]):
            for i in range(len(sequence)):
                yield seq[:i] + sequence[0:1] + seq[i:]

#base = [0,1,2,3,4]
base = [5,6,7,8,9][::-1]
seqs = give_sequence(base)

#input = '3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5'
#input = '3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10'
max_out = -1
#for sequence in [base]:
for sequence in seqs:
    curr = get_out(sequence)
    if curr > max_out:
        max_out = curr
        seq = sequence
        print(max_out)

print(seq, max_out)
